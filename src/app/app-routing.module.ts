import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListRecipesComponent } from './modules/list-recipes/list-recipes.component';
import { ShowRecipeComponent } from './modules/show-recipe/show-recipe.component';

const routes: Routes = [
  {path: '', component: ListRecipesComponent},
  {path: ':id', component: ShowRecipeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
