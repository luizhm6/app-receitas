import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { ListRecipesComponent } from './modules/list-recipes/list-recipes.component';
import { ContentComponent } from './components/content/content.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { HttpClientModule } from '@angular/common/http';
import {MatPaginatorModule} from '@angular/material/paginator';
import { ShowRecipeComponent } from './modules/show-recipe/show-recipe.component';
import {MatInputModule} from '@angular/material/input';
import { FormsModule }   from '@angular/forms';

@NgModule({
        declarations: [
                AppComponent,
                ListRecipesComponent,
                ContentComponent,
                SidebarComponent,
                HeaderComponent,
                ShowRecipeComponent
        ],
        imports: [
                BrowserModule,
                AppRoutingModule,
                BrowserAnimationsModule,
                HttpClientModule,
                MatSidenavModule,
                MatFormFieldModule,
                MatListModule,
                MatSelectModule,
                MatPaginatorModule,
                MatInputModule,
                FormsModule,
                ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
        ],
        providers: [],
        bootstrap: [AppComponent]
})
export class AppModule { }
