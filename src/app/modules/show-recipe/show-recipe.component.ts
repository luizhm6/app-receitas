import { Component, OnInit } from '@angular/core';
import { RecipesService } from '../recipes.service';

@Component({
  selector: 'app-show-recipe',
  templateUrl: './show-recipe.component.html',
  styleUrls: ['./show-recipe.component.scss']
})
export class ShowRecipeComponent implements OnInit {
  recipe: any;

  constructor(private service: RecipesService) { }

  ngOnInit(): void {
          this.recipe = this.service.selectedRecipe;
  }

}
