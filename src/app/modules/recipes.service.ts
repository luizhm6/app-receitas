import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
        providedIn: 'root'
})
export class RecipesService {
        
        public selectedRecipe: any;

        constructor(private http: HttpClient) { }

        getRecipes():any {
                return this.http.get('https://raw.githubusercontent.com/adrianosferreira/afrodite.json/master/afrodite.json')

        }

}
