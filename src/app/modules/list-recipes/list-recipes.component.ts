import { Component, OnInit, ViewChild } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { RecipesService } from '../recipes.service';

@Component({
        selector: 'app-list-recipes',
        templateUrl: './list-recipes.component.html',
        styleUrls: ['./list-recipes.component.sass']
})
export class ListRecipesComponent implements OnInit {
        rawRecipes: any[] = [];
        search = '';
        allRecipes: any[] = [];
        pageEvent: PageEvent;
        page: any;

        constructor(
                private service: RecipesService,
                private router: Router
        ) {
                this.pageEvent = new PageEvent();
        }

        ngOnInit(): void {
                this.page = { page: 1, length: this.allRecipes.length, pageRecipes: [] }
                this.service.getRecipes().subscribe(
                        (res: any) => {
                                this.allRecipes = res;
                                this.rawRecipes = res;
                                console.log(this.allRecipes);
                                this.changePage(0);

                        }
                )
        }

        changePage(pageNumber?: number) {
                if (!pageNumber && pageNumber !== 0) {
                        return;
                }
                this.page.pageRecipes = [];

                for (let index = pageNumber * 20; index < (pageNumber + 1) * 20; index++) {
                        const element = this.allRecipes[index];

                        this.page.pageRecipes.push(element);
                }
        }

        paginationEvent(event: any) {
                console.log(event)
                this.changePage(event.pageIndex);
        }

        selectRecipe(recipe: any) {
                console.log(recipe)
                this.service.selectedRecipe = recipe;
                this.router.navigateByUrl(recipe._id.$oid)
        }

        filterList(event: any) {
                console.log(event)
                this.allRecipes = []
                this.allRecipes = this.rawRecipes.filter(element => {
                        return element.nome.toLowerCase().includes(event.toLowerCase());
                })
                console.log(this.allRecipes)
                this.changePage(0);
        }

}
