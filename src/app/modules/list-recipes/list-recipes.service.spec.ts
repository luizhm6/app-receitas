import { TestBed } from '@angular/core/testing';

import { ListRecipesService } from './list-recipes.service';

describe('ListRecipesService', () => {
  let service: ListRecipesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListRecipesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
